/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.io.Serializable;

/**
 *
 * @author kanthapong
 */
public class Player implements Serializable {

    private char symbol;
    private int winCount, loseCount, drawCount;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public void win() {
        winCount++;
    }

    public void lose() {
        loseCount++;
    }

    public void draw() {
        drawCount++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount + '}';
    }

}
